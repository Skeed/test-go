package main

import (
	"context"
	"flag"
	"fmt"
	"github.com/jackc/pgx"
	"github.com/valyala/fasthttp"
	"log"
	"os"
)

var (
	addr     = flag.String("addr", ":8080", "TCP address to listen to")
	compress = flag.Bool("compress", false, "Whether to enable transparent response compression")
)

func main() {
	flag.Parse()

	h := requestHandler
	if *compress {
		h = fasthttp.CompressHandler(h)
	}

	if err := fasthttp.ListenAndServe(*addr, h); err != nil {
		log.Fatalf("Error in ListenAndServe: %s", err)
	}
}

func requestHandler(ctx *fasthttp.RequestCtx) {
	conn, err := pgx.Connect(context.Background(), "host=185.98.87.222 port=5432 dbname=skeed user=skeed password=pass")
	if err != nil {
		fmt.Fprintf(ctx, "Unable to connection to database: %v\n", err)
		os.Exit(1)
	} else {
		fmt.Fprintf(ctx, "Connect True!\n\n")
	}
	defer conn.Close(context.Background())

	name := string(ctx.QueryArgs().Peek("name"))

	if name == "german" || name == "German" {
		fmt.Fprintf(ctx, "Huesosnie names pisat ne budu!")
	} else {
		if name != "" {
			_, err = conn.Exec(context.Background(), "insert into users(name) values ($1)", name)
			fmt.Fprintf(ctx, "New name - %s\n\n", name)
		}

		fmt.Fprintf(ctx, "Names list:\n")

		if err != nil {
			fmt.Fprintf(os.Stderr, "Insert row failed: %v\n", err)
			os.Exit(1)
		}

		rows, err := conn.Query(context.Background(), "select name from users")
		if err != nil {
			fmt.Fprintf(os.Stderr, "QueryRow failed: %v\n", err)
			os.Exit(1)
		}

		for rows.Next() {
			var name string
			rows.Scan(&name)

			fmt.Fprintf(ctx, "· %s \n", name)
		}
	}

	ctx.SetContentType("text/plain; charset=utf8")

	// Set arbitrary headers
	ctx.Response.Header.Set("X-My-Header", "my-header-value")

	// Set cookies
	var c fasthttp.Cookie
	c.SetKey("cookie-name")
	c.SetValue("cookie-value")
	ctx.Response.Header.SetCookie(&c)
}
